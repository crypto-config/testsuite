#!/bin/bash

set -e
set -u
shopt -s inherit_errexit

# TODO: remove $force and use a random name for each new instance while making
# sure a trap cleans newly-created instances
force='true'

SILENT_APT='/dev/null'
# SILENT_APT='/dev/stdout'

wait_for_instance() {
  local instance
  local i=0

  instance="$1"

  while ((i < 20)); do
    if lxc exec "${instance}" -- systemctl is-system-running --wait >/dev/null 2>/dev/null; then
      break
    fi
    sleep 0.5
    : $((i = i + 1))
  done

  lxc exec "${instance}" -- systemctl is-system-running --wait
}

run_tool() {
  local name

  local tool

  name="$1"

  tools="$(find "${package_dir}" -regextype egrep -maxdepth 1 -type f -regex ".*/${name}(\.[0-9]*)?" -printf '%P\n')"

  for tool in ${tools}; do
    tool="$(cat "${package_dir}/${tool}")"

    lxc exec "${instance}" -- ${tool}
  done

}

run_test() {
  local package

  local hardening_dir_remote
  local instance
  local packaes
  local package_dir
  local tool

  package="$1"

  hardening_dir_remote="/root/hardening"
  instance="noble-hardening-${package}"
  package_dir="packages/${package}"

  packages=
  if [[ -e "packages/${package}/apt_install" ]]; then
    packages="$(cat "packages/${package}/apt_install")"
  else
    packages="${package}"
  fi

  if ${force}; then
    lxc stop "${instance}" || true
    lxc delete "${instance}" || true
  fi

  lxc launch 'ubuntu-daily:noble' "${instance}"
  wait_for_instance "${instance}"

  lxc exec "${instance}" -- \
    apt-get update >${SILENT_APT}
  lxc exec "${instance}" -- \
    apt-get install -y auto-apt-proxy eatmydata >${SILENT_APT}
  lxc exec "${instance}" --env NEEDRESTART_MODE=l -- \
    eatmydata \
    apt-get -y upgrade --autoremove \
      ssl-cert sslscan ssh-audit \
      ${packages} \
      >${SILENT_APT}

  lxc stop "${instance}"
  lxc start "${instance}"
  wait_for_instance "${instance}"

  lxc file push -rp "${package_dir}" "${instance}${hardening_dir_remote}"

  if [[ -e "${package_dir}/install-hooks" ]]; then
    lxc exec "${instance}" -- \
      run-parts --verbose --debug --exit-on-error \
        --arg="${hardening_dir_remote}/${package}" \
        "${hardening_dir_remote}/${package}/install-hooks"

    case "${package}" in
      python3|python3-openssl) ;;
      *) lxc exec "${instance}" -- systemctl restart "${package}" ;;
    esac
  fi

  for tool in 'sslscan' 'ssh-audit'; do
    run_tool "${tool}"
  done

  lxc stop --force "${instance}"
  # lxc delete "${instance}"
}

if (( ${#@} < 1 )); then
  packages="$(find packages -maxdepth 1 -mindepth 1 -type d -printf '%P\n' | sort)"
else
  packages="$@"
fi

printf -- 'Testing:\n'
printf -- '- %s\n' "${packages}"

for package in ${packages}; do
  cat << EOF

************************************************************
*   Testing ${package}   *
************************************************************

EOF
  case "${package}" in
    cups)
      # container quickly breaks and I don't know why
      continue ;;
    libvirt-daemon-system)
      # Requires creating a CA first:
      # https://wiki.libvirt.org/TLSCreateCACert.html
      continue ;;
    *)
      ;;
  esac
  run_test "${package}"
  cat << EOF

************************************************************
*   Done testing ${package}   *
************************************************************

EOF
done
