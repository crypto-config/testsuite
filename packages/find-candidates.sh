#!/bin/bash

set -e
set -u
shopt -s inherit_errexit

contents_files="/var/lib/apt/lists/fr.archive.ubuntu.com_ubuntu_dists_mantic_Contents-amd64.lz4 /var/lib/apt/lists/fr.archive.ubuntu.com_ubuntu_dists_mantic-updates_Contents-amd64.lz4 /var/lib/apt/lists/fr.archive.ubuntu.com_ubuntu_dists_mantic-backports_Contents-amd64.lz4 /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_mantic-security_Contents-amd64.lz4"

cat_apt_contents() {
  /usr/lib/apt/apt-helper cat-file ${contents_files}
}

get_providers_dependents() {
  apt-rdepends --reverse 'libgnutls30' 'libssl3' 2>/dev/null \
     | awk -F' ' '/Reverse Depends/ { print $3 }' \
     | sort \
     | uniq
}

providers_dependents="$(get_providers_dependents)"

cat_apt_contents \
  | awk -F' ' '$NF !~ /(universe|multiverse|restricted)/ { print $0 }' \
  | awk -F' ' '$1 ~ /systemd.*\.service/ { print $NF }' \
  | cut -f2 -d'/' \
  | sort \
  | uniq \
  | while read -r p; do
      case "${p}" in
        ec2*) continue ;;
        *) ;;
      esac

      if ! echo "${providers_dependents}" | grep -q -F -x "${p}"; then
        continue
      fi

      if apt-cache show "${p}" | grep -q -e OpenStack -e Ceph; then
        continue
      fi

      apt-cache show "${p}"
      # cat_apt_contents | mawk -F' ' "/\/${p}\$/ { print \$1 }"
      cat_apt_contents | rg "${p}\$"

      echo; echo; echo; echo; echo
    done
