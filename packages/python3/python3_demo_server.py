#!/usr/bin/python3

# From https://stackoverflow.com/a/77512705

import http.server
import ssl


def get_ssl_context(certfile, keyfile):
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    context.load_cert_chain(certfile, keyfile)
    # Obviously I won't leave what's below for testing but it shows what people
    # do in practice!
    # context.set_ciphers("@SECLEVEL=1:ALL")
    return context


class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers["Content-Length"])
        post_data = self.rfile.read(content_length)
        print(post_data.decode("utf-8"))


server_address = ("127.0.0.1", 5000)
httpd = http.server.HTTPServer(server_address, MyHandler)

context = get_ssl_context("/etc/ssl/certs/ssl-cert-snakeoil.pem",
                          "/etc/ssl/private/ssl-cert-snakeoil.key")
httpd.socket = context.wrap_socket(httpd.socket, server_side=True)

httpd.serve_forever()
