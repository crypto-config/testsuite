#!/usr/bin/python3

import socket
from OpenSSL import SSL


context = SSL.Context(SSL.TLS_METHOD)
context.use_privatekey_file("/etc/ssl/private/ssl-cert-snakeoil.key")
context.use_certificate_file("/etc/ssl/certs/ssl-cert-snakeoil.pem")

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('127.0.0.1', 5000))
sock.listen(10)

while True:
    client_sock, _ = sock.accept()
    try:
        ssl_conn = SSL.Connection(context, client_sock)
        ssl_conn.set_accept_state()
        ssl_conn.do_handshake()
        ssl_conn.set_shutdown()
    except:
        pass
    client_sock.close()
